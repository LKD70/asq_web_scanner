# asq_wb_scanner

This is a small command line based scanner.
It scans for Official ARK servers and saves them in a format usable by [asq-web](https://gitlab.com/LKD70/asq-web).

## Requirements: 
[Node.js](https://nodejs.org)

## Installation:
```
git clone https://gitlab.com/LKD70/asq_web_scanner.git
cd asq_web_scanner
npm i
```

## Usage:
```
node scan.js
```