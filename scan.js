'use strict';

const
	chalk = require('chalk'),
	fs = require('fs'),
	gamedig = require('gamedig'),
	ProgressBar = require('progress'),
	rp = require('request-promise-native');

const input = { servers: [] };
const ports = [ 27015, 27017, 27019, 27021, 27023 ];
const options = {
	uri: 'http://arkdedicated.com/officialservers.ini'
};

const scanFinished = () => {
	const server_count = input.servers.length;
	input.servers = input.servers.filter((server, i, s) => s.findIndex(t => t.steam_id === server.steam_id) === i);
	console.log(`Scan completed.`);
	if (input.servers.length < server_count) console.log(`Removed ${server_count - input.servers.length} duplicate entries.`);
	const output = { json: { servers: {} }, sql: [] };
	input.servers.forEach((server) => {
		output.json.servers[server.id] = server;
	});
	output.sql.push(input.servers.map((server) =>
		`INSERT INTO 'SERVERS' (${
			Object.keys(server).filter((key) => key).map((key) => {
				if (key === Object.keys(server)[Object.keys(server).length - 1]) return key;
				return key + ', ';
			}).join('')
		}) VALUES (${
			Object.values(server).filter((value) => value).map((value) => {
				if (value === Object.values(server)[Object.values(server).length - 1]) return value;
				return (typeof value === 'string' ? '"' + value + '"' : value) + ', ';
			}).join('')
		});`).join('\t\n'));
	console.log(`Saving results to 'servers.json' and 'servers.sql'.`);
	fs.writeFile('servers.json', JSON.stringify(output.json, null, 4), (err) => {
		if (err) {
			console.log(`An error occured whilst saving the JSON file: ${err}`);
		} else {
			console.log(`File saved.`);
		}
	});
	fs.writeFile('servers.sql', output.sql.join('\r\n'), (err) => {
		if (err) {
			console.log(`An error occured whilst saving the SQL file: ${err}`);
		} else {
			console.log(`File saved.`);
		}
	});
};

rp(options).then((res) => {
	const servers = res.split('\n').map((line) => {
		const line_elements = line.split(' //');
		if (line_elements.length > 1) {
			if (typeof line_elements !== 'undefined') {
				return line_elements;
			}
		}
		return null;
	});


	let hit_count = 0;
	const bar = new ProgressBar(
		`Found ${chalk.blue(':hit')} Servers [:bar] ${chalk.blue(':current/:total')}`,
		{
			complete: chalk.green('='),
			hit: hit_count,
			incomplete: chalk.red('-'),
			total: servers.length * ports.length
		}
	);

	servers.forEach((server) => {
		ports.map((port) => gamedig.query({
			attemptTimeout: 60000,
			host: server[0],
			maxAttempts: 6,
			port,
			socketTimeout: 15000,
			type: 'protocol-valve'
		}).then((state) => {
			const details = {
				battleeye: state.raw.rules.SERVERUSESBATTLEYE_b,
				cluster: state.raw.rules.ClusterId_s,
				game_port: state.raw.port,
				id: '',
				ip: server[0],
				legacy: state.raw.rules.LEGACY_i === '1',
				map: state.map,
				maxplayers: state.maxplayers,
				name: state.name,
				pretty_name: state.name.split(' ')[0],
				query_port: port,
				steam_id: state.raw.steamid,
				type:
						state.raw.rules.ClusterId_s === 'PCPVE' ? 'PvE'
							: state.raw.rules.ClusterId_s === 'NewPCPVE' ? 'PvE'
								: state.raw.rules.ClusterId_s === 'PCPVP' ? 'PvP'
									: state.raw.rules.ClusterId_s === 'NewPCPVP' ? 'PvP'
										: state.raw.rules.ClusterId_s === 'PCSmallTribes' ? 'SmallTribes'
											: state.raw.rules.ClusterId_s === 'PCArkpocalypse' ? 'Arkpocalypse'
												: state.raw.rules.ClusterId_s === 'NewPCHardcorePVP' ? 'Hardcore'
													: state.raw.rules.ClusterId_s === 'PCPrimPVE' ? 'Primitive'
														: state.raw.rules.ClusterId_s
			};
			details.id = state.raw.rules.LEGACY_i === '1' ? 'LEG_' : '';
			details.id += state.raw.rules.ClusterId_s === 'PCSmallTribes' ? 'ST_' : '';
			details.id += state.raw.rules.ClusterId_s === 'PCArkpocalypse' ? 'POC_' : '';
			details.id += state.raw.rules.ClusterId_s === 'NewPCHardcorePVP' ? 'HC_' : '';
			details.id += state.raw.rules.ClusterId_s === 'PCPrimPVE' ? 'PRIM_' : '';
			details.id += (state.map === 'ScorchedEarth' ? 'SE'
				: state.map === 'TheIsland' ? 'Island'
					: state.map === 'Ragnarok' ? 'Rag'
						: state.map === 'TheCenter' ? 'Center'
							: state.map === 'Aberration' ? 'Ab'
								: state.map === 'Extinction' ? 'Ext'
									: 'Unknown') + state.raw.rules.CUSTOMSERVERNAME_s.match(/(\d+)(?!.*\d)/)[0];
			if (server === servers[servers.length - 1] && port === ports[ports.length - 1]) {
				bar.terminate();
				scanFinished();
			} else {
				bar.tick({ hit: hit_count++ });
			}
			input.servers.push(details);
		}).catch(() => { // eslint-disable-line handle-callback-err
			if (server === servers[servers.length - 1] && port === ports[ports.length - 1]) {
				bar.terminate();
				scanFinished();
			} else {
				bar.tick();
			}
		}));
	});
});
